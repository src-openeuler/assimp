Name:          assimp
Version:       5.3.1
Release:       8
Summary:       Library to load and process various 3D model formats into applications.
License:       BSD and MIT and LGPL-2.1 and LGPL-2.0 and GPL-2.0 and LGPL-3.0 and GPL-3.0
URL:           http://www.assimp.org/
#wget https://github.com/assimp/assimp/archive/v%{version}.tar.gz
#tar xf v%{version}.tar.gz
#cd assimp-%{version}
#rm -rf test/models-nonbsd
#cd ..
#tar czf assimp-%{version}-free.tar.xz assimp-%{version}
Source0:       assimp-%{version}-free.tar.xz

Patch01:       CVE-2024-40724-Fix-out-of-bound-access-5651.patch
Patch02:       CVE-2024-45679.patch
Patch03:       CVE-2024-48425.patch
Patch04:       CVE-2024-48423-pre-Fix-leak-5762.patch
Patch05:       CVE-2024-48423.patch
Patch06:       CVE-2024-48424.patch
Patch07:       CVE-2024-53425-pre-Fix-Add-check-for-invalid-input-argument.patch
Patch08:       CVE-2024-53425.patch

BuildRequires: gcc-c++ boost-devel cmake dos2unix irrlicht-devel irrXML-devel
BuildRequires: doxygen poly2tri-devel gtest-devel pkgconfig(zziplib)
BuildRequires: pkgconfig(zlib) pkgconfig(minizip) gmock-devel make
BuildRequires: pkgconfig(python3) python3-rpm-macros
Provides:      bundled(polyclipping) = 4.8.8 bundled(openddl-parser)
Obsoletes:     %{name}-help < %{version}-%{release}

%description
Assimp is a library to load and process geometric scenes from various data formats.
Assimp aims to provide a full asset conversion pipeline for use in game
engines and real-time rendering systems of any kind, but is not limited
to this purpose.

%package       devel
Summary:       Headers and libraries for assimp
Requires:      assimp = %{version}-%{release}
Requires:      zlib-devel%{?_isa}
Requires:      pkgconfig(minizip)

%description devel
This package provides the header files and libraries
for assimp. Developers use it to develop programs.

%package -n    python3-assimp
Summary:       Python3 bindings for assimp
BuildArch:     noarch
Requires:      assimp = %{version}-%{release} python3
Provides:      assimp-python3 = %{version}-%{release}
Obsoletes:     assimp-python3 < 3.1.1

%description -n python3-assimp
This package provides the PyAssimp3 python bindings

%prep
%autosetup -n assimp-%{version} -p1

%build
%cmake -DASSIMP_BUILD_ASSIMP_TOOLS=TRUE \
       -DASSIMP_BUILD_ZLIB=OFF
       
%cmake_build

%install
%cmake_install
install -d %{buildroot}%{python3_sitelib}/pyassimp/
install -m 0644 port/PyAssimp/pyassimp/*.py %{buildroot}%{python3_sitelib}/pyassimp/

%files
%license LICENSE
%doc CREDITS
%{_libdir}/*.so.*
%{_bindir}/assimp

%files devel
%{_includedir}/assimp
%{_libdir}/*.so
%{_libdir}/pkgconfig/*.pc
%{_libdir}/cmake/*

%files -n python3-assimp
%doc port/PyAssimp/README.md
%{python3_sitelib}/pyassimp

%changelog
* Tue Feb 11 2025 yaoxin <1024769339@qq.com> - 5.3.1-8
- Fix CVE-2024-48423,CVE-2024-48424 and CVE-2024-53425

* Sun Feb 02 2025 Funda Wang <fundawang@yeah.net> - 5.3.1-7
- add requires on zlib and minizip devel pacakge, as it was referenced by
  pkgconfig and cmake description files
- drop help sub package, as it conflicts with main package

* Tue Oct 29 2024 xuezhixin <xuezhixin@uniontech.com> - 5.3.1-6
- use the new cmake macros

* Sat Oct 26 2024 liningjie <liningjie@xfusion.com> - 5.3.1-5
- Fix CVE-2024-48425

* Thu Sep 19 2024 wangkai <13474090681@163.com> - 5.3.1-4
- fix CVE-2024-45679

* Mon Jul 22 2024 yinyongkang <yinyongkang@kylinos.cn> - 5.3.1-3
- fix CVE-2024-40724

* Thu Mar 28 2024 Ge Wang <wang__ge@126.com> - 5.3.1-2
- Add executable binary file assimp

* Wed Oct 18 2023 chenyaqiang <chengyaqiang@huawei.com> - 5.3.1-1
- update to 5.3.1

* Sat Feb 04 2023 wenchaofan <349464272@qq.com> - 5.2.5-1
- Update to 5.2.5 version

* Wed Jun 15 2022 wulei <wulei80@h-partners.com> - 5.2.4-1
- Upgrade to 5.2.4

* Mon Jan 4 2021 Ge Wang <wangge20@huawei.com> - 3.3.1-22
- Modify homepage url and license infomation

* Fri Dec 25 2020 wangxiao <wangxiao65@huawei.com> - 3.3.1-21
- Remove unnessary BuildRequire DevIL

* Wed Oct 21 2020 chengzihan <chengzihan2@huawei.com> - 3.3.1-20
- Remove Subpackage python2-assimp

* Wed Sep 9 2020 Ge Wang <wangge20@huawei.com> - 3.3.1-19
- Modify Source0 Url

* Fri Feb 14 2020 likexin <likexin4@huawei.com> - 3.3.1-18
- Package init
